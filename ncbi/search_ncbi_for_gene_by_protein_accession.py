"""
This script is used to search for a Gene given a protein accession. It was used
to add gene symbol, gene description and ensembl ids to an existing file. 
However, it can be adjusted to do other things...

The implementation uses Biopython package and searches Entrez for a given
gene.
"""

from Bio import Entrez

def search(term):
	Entrez.email = 'some-mail@gmail.com'
	handle = Entrez.esearch(db='gene', term=term)
	record = Entrez.read(handle)

	if len(record['IdList']) == 0:
		return None

	ids = ','.join(record['IdList'])
	handle = Entrez.efetch(db='gene', id=ids, retmode='xml')

	records = Entrez.read(handle)
	results = []

	for record in records:
		symbol = record['Entrezgene_gene']['Gene-ref']['Gene-ref_locus']
		description = record['Entrezgene_gene']['Gene-ref']['Gene-ref_desc']

		gene_ids = []
		if 'Gene-ref' not in record['Entrezgene_gene']:
			print(record)

		if 'Gene-ref_db' in record['Entrezgene_gene']['Gene-ref']:
			db_ids = record['Entrezgene_gene']['Gene-ref']['Gene-ref_db']
			for db_id in db_ids:
				gene_ids.append({
					'id': db_id['Dbtag_tag']['Object-id']['Object-id_str'],
					'source': db_id['Dbtag_db']
				})

		results.append({
			'symbol': symbol,
			'description': description,
			'ids': gene_ids
		})

	return results

def new_header():
	return '\t'.join([
		'#',
		'Identified Proteins (293)',
		'Accession Number',
		'Gene Symbol',
		'Gene Description',
		'Ensembl ID',
		'Molecular Weight',
		'Sample3',
		'Sample4',
		'Sample5',
		'Sample6',
		'Sample7',
		'Sample8',
		'Sample9',
		'Sample10'
	])


def get_new_line(old_line, genes):
	symbol = ""
	description = ""
	ids = ""
	if genes:
		symbol = genes[0]['symbol']
		description = genes[0]['description']

		ensembl_ids = []
		for gene_id in genes[0]['ids']:
			if gene_id['source'].lower() == 'ensembl':
				ensembl_ids.append(gene_id['id'])

		ids = ','.join(ensembl_ids)

	old_line[3] = symbol
	old_line.insert(4, description)
	old_line.insert(5, ids)

	return '\t'.join(old_line)

def main():
	with open('/storage/blah.tsv') as f:
		with open('/storage/blah2.tsv', 'w') as o:
			o.write(new_header() + '\n')

			for line in f:
				data = line.strip().split('\t')

				if line.startswith('#'):
					continue

				if len(data) < 3:
					continue

				accession = data[2]
				accession = accession.split()
				results = search(accession[0])

				o.write(get_new_line(data, results) + '\n')			


if __name__ == '__main__':
	main()