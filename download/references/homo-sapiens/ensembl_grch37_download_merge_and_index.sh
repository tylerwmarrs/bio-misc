#!/bin/bash

# Download all of the chromosomes 1 - 22, X, Y, MT
for i in {1..22};
do
	echo "Downloading chr $i"
	curl -O ftp://ftp.ensembl.org/pub/release-75/fasta/homo_sapiens/dna/Homo_sapiens.GRCh37.75.dna.chromosome.$i.fa.gz; 
done

echo "Downloading chr X"
curl -O ftp://ftp.ensembl.org/pub/release-75/fasta/homo_sapiens/dna/Homo_sapiens.GRCh37.75.dna.chromosome.X.fa.gz
echo "Downloading chr Y"
curl -O ftp://ftp.ensembl.org/pub/release-75/fasta/homo_sapiens/dna/Homo_sapiens.GRCh37.75.dna.chromosome.Y.fa.gz
echo "Downloading chr Z"
curl -O ftp://ftp.ensembl.org/pub/release-75/fasta/homo_sapiens/dna/Homo_sapiens.GRCh37.75.dna.chromosome.MT.fa.gz

# Extract and remove gunzip files
for f in *.gz;
do
	echo "Extracting $f"
	gunzip $f
done

# Merge all fasta files into one file
for i in {1..22};
do
	echo "Merging chr $i"
	cat "Homo_sapiens.GRCh37.75.dna.chromosome.$i.fa" >> Homo_sapiens.GRCh37.75.dna.fa
done

echo "Merging chr X"
cat Homo_sapiens.GRCh37.75.dna.chromosome.X.fa >> Homo_sapiens.GRCh37.75.dna.fa
echo "Merging chr Y"
cat Homo_sapiens.GRCh37.75.dna.chromosome.Y.fa >> Homo_sapiens.GRCh37.75.dna.fa
echo "Merging chr MT"
cat Homo_sapiens.GRCh37.75.dna.chromosome.MT.fa >> Homo_sapiens.GRCh37.75.dna.fa

# Index the merged fasta
echo "Indexing..."
samtools faidx Homo_sapiens.GRCh37.75.dna.fa