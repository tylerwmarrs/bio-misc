#!/bin/bash

# Download all of the chromosomes 1 - 19, X, Y, MT
for i in {1..19};
do
	echo "Downloading chr $i"
    curl -O ftp://ftp.ensembl.org/pub/release-85/fasta/mus_musculus/dna/Mus_musculus.GRCm38.dna.chromosome.$i.fa.gz
done

echo "Downloading chr X"
curl -O ftp://ftp.ensembl.org/pub/release-85/fasta/mus_musculus/dna/Mus_musculus.GRCm38.dna.chromosome.X.fa.gz
echo "Downloading chr Y"
curl -O ftp://ftp.ensembl.org/pub/release-85/fasta/mus_musculus/dna/Mus_musculus.GRCm38.dna.chromosome.Y.fa.gz
echo "Downloading chr MT"
curl -O ftp://ftp.ensembl.org/pub/release-85/fasta/mus_musculus/dna/Mus_musculus.GRCm38.dna.chromosome.MT.fa.gz

# Extract and remove gunzip files
for f in *.gz;
do
	echo "Extracting $f"
	gunzip $f
done

# Merge all fasta files into one file
for i in {1..19};
do
	echo "Merging chr $i"
    cat "Mus_musculus.GRCm38.dna.chromosome.$i.fa" >> Mus_musculus.GRCm38.dna.fa
done

echo "Merging chr X"
cat Mus_musculus.GRCm38.dna.chromosome.X.fa >> Mus_musculus.GRCm38.dna.fa
echo "Merging chr Y"
cat Mus_musculus.GRCm38.dna.chromosome.Y.fa >> Mus_musculus.GRCm38.dna.fa
echo "Merging chr MT"
cat Mus_musculus.GRCm38.dna.chromosome.MT.fa >> Mus_musculus.GRCm38.dna.fa

# Index the merged fasta
echo "Indexing..."
samtools faidx Mus_musculus.GRCm38.dna.fa
