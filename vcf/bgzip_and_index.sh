#!/bin/bash
set -e

function usage {
cat <<EOF
Usage: $0 [options]

-h|--help     display help
-i|--input    vcf file as input
EOF
}

while [ "$1" != "" ]; do
    PARAM=`echo $1 | awk -F= '{print $1}'`
    VALUE=`echo $1 | awk -F= '{print $2}'`
    case $PARAM in
        -h | --help)
            usage
            exit
            ;;
        -i | --input)
            INPUT=$VALUE
            ;;
        *)
            echo "ERROR: unknown parameter \"$PARAM\""
            usage
            exit 1
            ;;
    esac
    shift
done

bgzip -c $INPUT > "$INPUT.gz"
tabix -p vcf "$INPUT.gz"
