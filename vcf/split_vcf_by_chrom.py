#! /usr/bin/env python

# This script splits a VCF into multiple files by chromosome. It keeps the 
# VCF header from the original file and can read from compressed files.

import argparse
import gzip
import os
import mimetypes

def parse_args():
    """
    Parses the command line arguments.
    """
    parser = argparse.ArgumentParser(description='Split VCF by chromosome')
    parser.add_argument('input', type=str, help='Input VCF to split')
    parser.add_argument('output', type=str, help='Output directory')

    return parser.parse_args()

def is_gzip(file_path):
    """
    Helper to determine if file is gzipped or not.
    """
    return 'gzip' in mimetypes.guess_type(file_path)

def create_output_directory(output_path):
    """
    Helper to create output directory if it does not exist.
    """
    if not os.path.exists(output_path):
        os.mkdir(output_path)

def split_vcf(file_path, output_path):
    """
    Splits VCF by chrom into current directory.
    """
    header_lines = []
    file_handle = None

    if is_gzip(file_path):
        file_handle = gzip.open(file_path, 'rt')
    else:
        file_handle = open(file_path, 'rt')

    chrom = None
    chrom_file = None
    for line in file_handle:
        if line.startswith('#'):
            header_lines.append(line)
        else:
            data = line.split('\t')
            if data[0] != chrom:

                if chrom_file is not None:
                    chrom_file.close()

                chrom = data[0]
                chrom_file_path = os.path.join(output_path, chrom + '.vcf')
                chrom_file = open(chrom_file_path, 'w')

                for header in header_lines:
                    chrom_file.write(header)
            else:
                chrom_file.write(line)

    chrom_file.close()
    file_handle.close()

def main():
    args = parse_args()
    create_output_directory(args.output)
    split_vcf(args.input, args.output)

if __name__ == "__main__":
    main()
